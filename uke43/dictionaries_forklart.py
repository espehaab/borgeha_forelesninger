# Ta en kikk paa passordliste.py, denne viser
# hvordan man kan bruke en todimensjonal liste.


# Dictionaries
'''
En dictionary kan sees på som en kommode med mange
skuffer. Utenpå hver skuffe står det en nøkkel (key).
Nøkkelen kan være hva som helst som er ikke-muterbart,
det vil si at man for eksempel ikke kan bruke en liste
som en nøkkel.

Denne
må være unik (akkurat som sett). Hver skuffe har også en
verdi (value). Alt mulig kan ligge inni skuffen. En boolean,
et talln en todimensjonal liste, en annen dictionary...
Mulighetene er uendelige!

Dette er for øvrig ikke en fullstendig beskrivelse av
dictionaries, men det viser de mest brukte tingene ved dem.
'''

# Lage en tom dictionary:
ordbok = {}

# Legge inn en verdi 12 til nøkkelen 'svaret'
ordbok['svaret'] = 12
print('svaret: ',ordbok)
# Den samme ordbokn kan ha ulike typer på nøkler og verdier
ordbok[12] = 'tre ganger fire'
ordbok['liste'] = [[1,2],[3,4]]
print('tre elementer: ',ordbok)
# Bare én nøkkel av hver type. Setter du den igjen overskriver du
ordbok[12] = 'tolv ganger én'
print('Ny tolver: ',ordbok)

# For å hente ut en verdi gitt en nøkkel:
print(f'Inni skuffen med "liste" utenpå ligger {ordbok["liste"]}')

# Man kan hente ut alle nøkler (skuffefronter) slik:
for key in ordbok.keys():
    print('nøkkel:',key)
    # Men man kan også hente ut verdiene, gitt key
    print('nøkkel:',key,'har verdi:',ordbok[key])
    
# Man kan hente ut alle verdier (inni skuffene) slik:
for value in ordbok.values():
    print('verdi:',value)

# Man kan hente begge slik:
for key, value in ordbok.items():
    print('verdi:',value)

# Hvis en forsøker å hente ut en verdi på denne måten, og
# nøkkelen ikke finnes, da utløses et unntak:
# print(ordbok['finnesikke'])
# Dette kan man unngå ved å bruke get(<nøkkel>):
print('bruke get:',ordbok.get(12))

# Hvis nøkkelen ikke finnes, da returnerer get None:
print('bruke get på ikkeeksisterende nøkkel:',ordbok.get('finnesikke'))

# Man kan i stedet la den returnere noe en selv spesifiserer hvis
# nøkkelen ikke finnes, med get(<nøkkel>,<retur_hvis_ikke_eksisterer>)
print('bruke get med bestemt retur:',ordbok.get('finnesikke',[]))


# Eksempel med bruk v dictionary og analyse av en lenger streng:
# Lag en ordbok som teller antall forekomster av alle ord.
# ordet selv er nøkkelen, antall ganger er verdien knyttet
# til denne.


ordbok.clear() # Nå er den tom

import lyric
# I filen lyric.py ligger en strengvariabel TEKST
for ord in lyric.TEKST.split(): # Sjekk bruken av modulen
#    print(f'"{ord}"')
    # Nå må vi finne ut hvor mange ganger ordet har vært
    # talt opp før. Hvis det er første gang ber vi get
    # returnere 0. Smart, ikke sant?

    # Fjerne noen punktum og slikt.
    ord = ord.replace('.','')
    ord = ord.replace(')','')
    ord = ord.replace('(','')
    ord = ord.replace(',','')
    
    ganger = ordbok.get(ord,0)
    
    # Så oppdaterer vi den med 1
    ordbok[ord] = ganger + 1
    # kunne gjort det på en linje:
#    ordbok[ord] = ordbok.get(ord,0) + 1

# Skriv ut alle ordene og antall treff, i alfabetisk rekkefølge:
print('Ord:\t\tGanger:')
# sorted kan brukes på keys, values, under velger den automatisk keys:
for ord in sorted(ordbok): 
    print(ord,'\t\t',ordbok[ord])
